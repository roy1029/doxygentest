#include "pch.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <pdh.h>
#include <pdhmsg.h>

#pragma comment(lib, "pdh.lib")

/// @brief Sample interval in milliseconds.
CONST ULONG SAMPLE_INTERVAL_MS = 1000;
/// @brief Global flag indicating if the monitoring is running.
volatile BOOL g_bRunning = TRUE;

/// @brief Structure to hold data for each network interface.
typedef struct {
    LPWSTR szInstanceName; ///< Name of the network interface.
    double totalTx;       ///< Total transmitted data in Kbps.
    double totalRx;       ///< Total received data in Kbps.
} NetworkInterfaceData;

/**
 * @brief Main function to monitor network interfaces.
 */
void wmain(void) {
    PDH_STATUS Status;
    LPWSTR szCounterListBuffer = NULL;
    DWORD dwCounterListSize = 0;
    LPWSTR szInstanceListBuffer = NULL;
    DWORD dwInstanceListSize = 0;

    // Enumerate network interfaces.
    Status = PdhEnumObjectItems(NULL, NULL, L"Network Interface", szCounterListBuffer, &dwCounterListSize, szInstanceListBuffer, &dwInstanceListSize, PERF_DETAIL_WIZARD, 0);
    if (Status == PDH_MORE_DATA) {
        szCounterListBuffer = (LPWSTR)malloc(dwCounterListSize * sizeof(WCHAR));
        szInstanceListBuffer = (LPWSTR)malloc(dwInstanceListSize * sizeof(WCHAR));
        Status = PdhEnumObjectItems(NULL, NULL, L"Network Interface", szCounterListBuffer, &dwCounterListSize, szInstanceListBuffer, &dwInstanceListSize, PERF_DETAIL_WIZARD, 0);
    }

    // Count the number of interfaces.
    DWORD dwNumInterfaces = 0;
    for (LPWSTR szNextInstance = szInstanceListBuffer; *szNextInstance != L'\0'; szNextInstance += wcslen(szNextInstance) + 1) {
        dwNumInterfaces++;
    }

    // Allocate memory for interface data and counters.
    NetworkInterfaceData* pInterfaceData = (NetworkInterfaceData*)malloc(dwNumInterfaces * sizeof(NetworkInterfaceData));
    HQUERY* phQueries = (HQUERY*)malloc(dwNumInterfaces * sizeof(HQUERY));
    HCOUNTER* phCountersTx = (HCOUNTER*)malloc(dwNumInterfaces * sizeof(HCOUNTER));
    HCOUNTER* phCountersRx = (HCOUNTER*)malloc(dwNumInterfaces * sizeof(HCOUNTER));

    // Initialize interface data and counters.
    DWORD i = 0;
    for (LPWSTR szNextInstance = szInstanceListBuffer; *szNextInstance != L'\0'; szNextInstance += wcslen(szNextInstance) + 1) {
        pInterfaceData[i].szInstanceName = szNextInstance;
        pInterfaceData[i].totalTx = 0;
        pInterfaceData[i].totalRx = 0;

        Status = PdhOpenQuery(NULL, NULL, &phQueries[i]);
        if (Status != ERROR_SUCCESS) {
            wprintf(L"\nPdhOpenQuery failed with status 0x%x.", Status);
            continue;
        }

        WCHAR CounterPathBuffer[PDH_MAX_COUNTER_PATH];
        WCHAR CounterPathBuffer2[PDH_MAX_COUNTER_PATH];

        swprintf(CounterPathBuffer, PDH_MAX_COUNTER_PATH, L"\\Network Interface(%s)\\Bytes Sent/sec", szNextInstance);
        Status = PdhAddEnglishCounter(phQueries[i], CounterPathBuffer, 0, &phCountersTx[i]);
        if (Status != ERROR_SUCCESS) {
            wprintf(L"\nPdhAddCounter for TX failed with status 0x%x.", Status);
            continue;
        }

        swprintf(CounterPathBuffer2, PDH_MAX_COUNTER_PATH, L"\\Network Interface(%s)\\Bytes Received/sec", szNextInstance);
        Status = PdhAddEnglishCounter(phQueries[i], CounterPathBuffer2, 0, &phCountersRx[i]);
        if (Status != ERROR_SUCCESS) {
            wprintf(L"\nPdhAddCounter for RX failed with status 0x%x.", Status);
            continue;
        }

        i++;
    }

    // Continuously monitor and display network data.
    while (g_bRunning) {
        double totalTx = 0;
        double totalRx = 0;
        SYSTEMTIME SampleTime;

        for (DWORD i = 0; i < dwNumInterfaces; i++) {
            Status = PdhCollectQueryData(phQueries[i]);
            if (Status != ERROR_SUCCESS) {
                wprintf(L"\nPdhCollectQueryData failed with 0x%x.\n", Status);
                continue;
            }
        }

        Sleep(SAMPLE_INTERVAL_MS);

        for (DWORD i = 0; i < dwNumInterfaces; i++) {
            Status = PdhCollectQueryData(phQueries[i]);
            if (Status != ERROR_SUCCESS) {
                wprintf(L"\nPdhCollectQueryData failed with 0x%x.\n", Status);
                continue;
            }

            PDH_FMT_COUNTERVALUE DisplayValueTx;
            PDH_FMT_COUNTERVALUE DisplayValueRx;

            Status = PdhGetFormattedCounterValue(phCountersTx[i], PDH_FMT_DOUBLE, NULL, &DisplayValueTx);
            if (Status != ERROR_SUCCESS) {
                wprintf(L"\nPdhGetFormattedCounterValue for TX failed with status 0x%x.", Status);
                continue;
            }

            Status = PdhGetFormattedCounterValue(phCountersRx[i], PDH_FMT_DOUBLE, NULL, &DisplayValueRx);
            if (Status != ERROR_SUCCESS) {
                wprintf(L"\nPdhGetFormattedCounterValue for RX failed with status 0x%x.", Status);
                continue;
            }

            pInterfaceData[i].totalTx = DisplayValueTx.doubleValue * 8 / 1024;
            pInterfaceData[i].totalRx = DisplayValueRx.doubleValue * 8 / 1024;

            totalTx += pInterfaceData[i].totalTx;
            totalRx += pInterfaceData[i].totalRx;

            wprintf(L"Interface: %s, TX: %lf, RX: %lf\n", pInterfaceData[i].szInstanceName, pInterfaceData[i].totalTx, pInterfaceData[i].totalRx);
        }

        GetLocalTime(&SampleTime);

        wprintf(L"%2.2d/%2.2d/%4.4d %2.2d:%2.2d:%2.2d.%3.3d, Total TX: %lf, Total RX: %lf\n\n",
            SampleTime.wMonth,
            SampleTime.wDay,
            SampleTime.wYear,
            SampleTime.wHour,
            SampleTime.wMinute,
            SampleTime.wSecond,
            SampleTime.wMilliseconds,
            totalTx, totalRx);
    }

    // Cleanup and free allocated memory.
    for (DWORD i = 0; i < dwNumInterfaces; i++) {
        PdhCloseQuery(phQueries[i]);
    }

    free(phQueries);
    free(phCountersTx);
    free(phCountersRx);
    free(pInterfaceData);
    free(szCounterListBuffer);
    free(szInstanceListBuffer);
}
